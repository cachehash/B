%{
#include <stdlib.h>
#include "main.h"
//include token ids for Bison definitions
#include "parse.tab.h"


int yycolumn = 1;
//this is buggy if I recall
#define YY_USER_ACTION yylloc.first_line = yylloc.last_line = yylineno; \
	    yylloc.first_column = yycolumn; yylloc.last_column = yycolumn + yyleng - 1; \
	    yycolumn += yyleng;

char* parseString(char* str) {
	return strdup(str);
}

//below are regex definitions
%}
num	([0-9]+|0[0-7]+|0x[0-9a-fA-F]+)
float	[0-9]*\.[0-9]+
id	[a-zA-Z_.][a-zA-Z0-9_.]*

%option noyywrap
%option yylineno
%%


auto	{return AUTO;}
extrn	{return EXTRN;}

while	{return WHILE;}
if	{return IF;}
else	{return ELSE;}
switch	{return SWITCH;}
case	{return CASE;}
goto	{return GOTO;}

sizeof	{return SIZEOF;}

"/*"(\*+[^/]|[^*]|\n)*"*/"		{/*comment, do nothing. */}
\"(\\\"|[^"])*\"			{yylval.s = parseString(yytext); return STR_VAL;}

{id} {
	//must malloc new buffer and copy yytext into it
	yylval.s = strdup(yytext);
	return ID;
}

{num} {
	//the value of the number is converted to an int and passed to bison through yylval
	yylval.i = strtol(yytext,NULL,0);
	//tells bison that INT the symbol to process
	return INT_VAL;
}
{float}	{
	float f = atof(yytext);
	yylval.f = f;
	return FLOAT_VAL;
}
'\*x[0-9a-fA-F]+' {
	//substr skipping first 3 chars, then convert from hex. strtol should ignore garbage at the end (')
	yylval.c = strtol(yytext+3, NULL, 16);
	return CHAR_VAL;
}
'\*[0-7]+' {
	//octal to decimal
	yylval.c = strtol(yytext+2, NULL, 8);
	return CHAR_VAL;
}
'\*\*' {
	// '*' is the escape character in B instead of '\'
	yylval.c = '*';
	return CHAR_VAL;
}
'[^*]' {
	yylval.c = yytext[1];
	return CHAR_VAL;
}
'\*n'	{yylval.c = '\n'; return CHAR_VAL;}


"<="	{return LE;}
">="	{return GE;}
"=="	{return EQ;}
"!="	{return NE;}
"<<"	{return LSHIFT;}
">>"	{return RSHIFT;}
"++"	{return INC_OP;}
"--"	{return DEC_OP;}
"&&"	{return AND;}
"||"	{return OR;}

"=*"	{return STAR_EQ;}
"=/"	{return SLASH_EQ;}
"=%"	{return REM_EQ;}
"=+"	{return PLUS_EQ;}
"=-"	{return MINUS_EQ;}
"=&"	{return AND_EQ;}
"=^"	{return XOR_EQ;}
"=|"	{return OR_EQ;}
"=<<"	{return LSHIFT_EQ;}
"=>>"	{return RSHIFT_EQ;}




[-<>+*/=?:{}[\]()&|^%,;!] {
	//the one character symbols (bison tokens defined by their ascii values)
	return yytext[0];
}


[ \t\n]	{/*meaningless whitespace*/}
.	{
	char buff[2048];
	snprintf(buff, 2048, "Lexer: Unexpected character `%s'", yytext);
	yyerror(buff);
}


%%
