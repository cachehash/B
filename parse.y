%{
extern int yylex();
#include "main.h"
%}

%union {
	int i;
	float f;
	double d;
	char* s;
	char c;
}
//if-else constructs are intrinsically ambiguous.
%precedence ')'
%left ELSE

//declaration keyword (qualifier)
%token AUTO EXTRN


//control flow keywords
%token WHILE IF ELSE
%token SWITCH CASE
%token GOTO //the all powerful


//operators
%token LE GE EQ NE LSHIFT RSHIFT INC_OP DEC_OP AND OR STAR_EQ SLASH_EQ REM_EQ PLUS_EQ MINUS_EQ LSHIFT_EQ RSHIFT_EQ AND_EQ XOR_EQ OR_EQ SIZEOF

%token<i> INT_VAL
%token<f> FLOAT_VAL
%token<c> CHAR_VAL
%token<s> STR_VAL

%token<s> ID


%type<c> conditional_expression logical_or_expression logical_and_expression inclusive_or_expression exclusive_or_expression and_expression equality_expression relational_expression shift_expression additive_expression multiplicative_expression cast_expression unary_expression postfix_expression primary_expression constant expression assignment_expression assignment_operator unary_operator
%define parse.error verbose 
%locations
//expressions addapted from https://cs.wmich.edu/~gupta/teaching/cs4850/sumII06/The%20syntax%20of%20C%20in%20Backus-Naur%20form.htm
%%

program			: program_body

program_body		:
			| function_dec program_body 
			| ID '[' INT_VAL ']' ';' program_body 
			| ID INT_VAL ';' program_body 

id_list			: ID ',' id_list
			| ID

function_dec		: ID '(' id_list ')' code_body
			| ID '(' ')' code_body

code_body		: '{' declarations statement_list '}'
	  	
statement_list		:
			| statement statement_list 
			| ID ':'

statement		: expression ';'
			| GOTO ID ';'
			| code_body
			| WHILE '(' expression ')' statement
			| IF '(' expression ')' statement ELSE statement
			| IF '(' expression ')' statement
			| SWITCH expression '{' switch_body '}'
			| ID ':' statement

switch_body		: CASE expression ':' statement_list switch_body
			|




declarations		: qualifier id_list ';' declarations
			| 

qualifier		: AUTO
			| EXTRN

conditional_expression : logical_or_expression
                       | logical_or_expression '?' expression ':' conditional_expression

logical_or_expression : logical_and_expression
                      | logical_or_expression OR logical_and_expression

logical_and_expression : inclusive_or_expression
                       | logical_and_expression AND inclusive_or_expression

inclusive_or_expression : exclusive_or_expression
                        | inclusive_or_expression '|' exclusive_or_expression

exclusive_or_expression : and_expression
                        | exclusive_or_expression '^' and_expression

and_expression : equality_expression
               | and_expression '&' equality_expression

equality_expression : relational_expression
                    | equality_expression EQ relational_expression
                    | equality_expression NE relational_expression

relational_expression : shift_expression
                      | relational_expression '<' shift_expression
                      | relational_expression '>' shift_expression
                      | relational_expression LE shift_expression
                      | relational_expression GE shift_expression

shift_expression : additive_expression
                 | shift_expression LSHIFT additive_expression
                 | shift_expression RSHIFT additive_expression

additive_expression : multiplicative_expression
                    | additive_expression '+' multiplicative_expression
                    | additive_expression '-' multiplicative_expression

multiplicative_expression : cast_expression
                          | multiplicative_expression '*' cast_expression
                          | multiplicative_expression '/' cast_expression
                          | multiplicative_expression '%' cast_expression

cast_expression : unary_expression
//                | '(' type-name ')' cast_expression

unary_expression : postfix_expression						{}
                 | INC_OP unary_expression					{}
                 | DEC_OP unary_expression					{}
                 | unary_operator cast_expression				{}
//                 | SIZEOF unary_expression
//                 | SIZEOF type-name

postfix_expression : primary_expression
                   | postfix_expression '[' expression ']'
                   | postfix_expression '(' expression ')'
                   | postfix_expression '(' ')'
                   | postfix_expression INC_OP
                   | postfix_expression DEC_OP

primary_expression : ID				{}
                   | constant
                   | STR_VAL			{}
                   | '(' expression ')'		{}

constant : INT_VAL				{}
         | CHAR_VAL				{}
         | FLOAT_VAL				{}
//       | enumeration_constant			{}

expression : assignment_expression
           | expression ',' assignment_expression

assignment_expression : conditional_expression
                      | unary_expression assignment_operator assignment_expression

assignment_operator : '='					{}
                    | STAR_EQ					{}
                    | SLASH_EQ					{}
                    | REM_EQ					{}
                    | PLUS_EQ					{}
                    | MINUS_EQ					{}
                    | LSHIFT_EQ					{}
                    | RSHIFT_EQ					{}
                    | AND_EQ					{}
                    | XOR_EQ					{}
                    | OR_EQ					{}

unary_operator : '&'					{}
               | '*'					{}
               | '+'					{}
               | '-'					{}
               | '~'					{}
               | '!'					{}


