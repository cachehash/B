.PHONY: all clean .gitignore
srcs=lexer.c parse.tab.h parse.tab.c
objs=lexer.o parse.tab.o main.o
cleanable=$(srcs) $(objs) $(bin) parse.output

YYFLAGS=-v

#in honor of Ken Thompson, a nice short name for the executable
bin=bint

all:
	make -j bint

lexer.o main.o: parse.tab.h

$(bin): $(objs)
	$(CC) $(CFLAGS) $(CPPFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

%.tab.h %.tab.c: %.y
	bison -d $(YYFLAGS) $<

.gitignore:
	cat gitignore-HEAD > $@
	printf '%s\n' $(cleanable) >> $@


clean::
	rm -f -- $(cleanable)
