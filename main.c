#include <stdio.h>
#include "parse.tab.h"
#include "main.h"
#include <stdarg.h>
#include <stdlib.h>

int error_code = 0;
extern int yylineno;
extern char* yytext;
int yyerror(const char* err) {
	fprintf(stderr, "ERROR on line %d in `%s': %s\n", yylineno, yytext, err);
	error_code |= 1;
}

int main() {
	yyparse();
	return error_code;
}

Node* mkNode(YYLTYPE loc, int sz) {
	Node* ret;
	ret = calloc(sizeof(*ret)+sz*sizeof(*ret->d), 1);
	ret->sz = sz;
	ret->line = yylloc.first_line;
	ret->first_line = yylloc.first_line;
	ret->first_column = yylloc.first_column;
	ret->last_line = yylloc.last_line;
	ret->last_column = yylloc.last_column;
	return ret;
}
Node* buildNode(YYLTYPE loc, int sz, int type, ...) {
	va_list ap;
	va_start(ap, type);
	Node* ret = mkNode(loc, sz);
	ret->type = type;
	for (int i = 0; i < sz; i++) {
		ret->d[i].n = va_arg(ap, void*);
	}
	va_end(ap);
	return ret;
}
